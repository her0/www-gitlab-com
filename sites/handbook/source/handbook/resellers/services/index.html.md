---
layout: handbook-page-toc
title: "Channel Services Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
# GitLab Channel Services Program

The GitLab Channel Services program is designed to help new or existing partners design service portfolios around the DevOps Lifecycle in correlation to GitLab products. The program will help partners evaluate business opportunities and create a technical enablement framework to use as they build their GitLab-related service practices. 

The foundation of the GitLab Channel Services Program consists of two elements on which partners can build upon to start or grow their GitLab-related service portfolio:



1. <a href="https://about.gitlab.com/handbook/resellers/services/#gitlab-certified-service-partner-program-overview">Badges</a>:  GitLab offers GitLab Service Partner Badges, that include training for service delivery teams which enable partners to meet program compliance requirements. 
2. <a href="https://partners.gitlab.com/prm/English/c/Services">GitLab Partner Portal Services Page</a>:  Partners can utilize GitLab Channel Service Packages to assist their teams as they build the service offerings, market and sell their services, and support their service business growth. 
    <br>a. <a href="https://partners.gitlab.com/prm/English/c/Channel_Service_Packages">GitLab Channel Service Packages</a>
    <br>b. <a href="https://partners.gitlab.com/prm/English/c/GitLab_Channel_Service_Sales_GTM">GitLab Channel Service Sales Go To Market</a>
    <br>c. <a href="https://partners.gitlab.com/prm/English/c/GitLab_Channel_Service_Development_Framework">GitLab Channel Service Development Framework</a>

## GitLab Service Partner Program Overview

The GitLab Service Partner Program offers our Partners three Certifications:


<table>
  
  <tr>
   <td style = "text-align: center">




<img src="/images/channel-service-program/gitlab-professional-services-partner-badge.png" width="150" alt="" title="GitLab Professional Services Partner">

   </td>
   

  </tr>
  <tr>
   <td style = "text-align: center">GitLab Professional Services Partner (PSP)
<br> 
<br>Distinguished partners with validated service capabilities and a record for delivering excellent customer value through professional services.
   </td>
  </tr>
</table>



<br>We have designed the GitLab Channel Service Partner Program to help partners who focus on offering services as a key foundation in their business model. These services can include:  



*  Assessment
*  Migration
*  Integration
*  Implementation
*  Education
*  Optimization
*  Managed/hosted services
*  Security/compliance

Becoming an Open or Select GitLab partner or a GitLab Distribution partner is the first step to becoming a GitLab Service Partner and allows you to engage with the GitLab sales team and resell or refer GitLab products to your customers. Partners can achieve the GitLab Service Partner Program badges in addition to their program status to differentiate through their service offerings and unlock both financial and non-financial incentives as they achieve Service Partner Badges.

Whether you are a new partner just getting started with a service portfolio or you already have a thriving service business that you are looking to grow through offering services around GitLab, our GitLab Service Partner Program will offer you opportunities to build a successful business with GitLab.


## GitLab Service Partner Requirements and Progression

At GitLab, Service Partner Badges are meant to recognize service delivery excellence, technical expertise, and customer success among our partners for GitLab use cases in the DevOps lifecycle. Achieving a GitLab Service Partner Badge allows you to enhance your service capabilities and unlock key partner benefits in our program. 

![Service Program Requirements and Progression](/images/channel-service-program/service-program-requirements-and-progression.png)
<br>

## Becoming a Service Partner

All GitLab Service Partner tracks have program compliance and training requirements that must be completed before you can obtain the associated badge.

When the Open, Select, or Distribution compliance criteria are met, and your organization sponsors the required number of practitioners who complete the training requirements and obtain the associated certifications and accreditations, your company will earn the related GitLab Service Partner Badge. 

The GitLab Channel team will communicate the award in email, and reflect the certification in the GitLab Partner Locator. 

All GitLab Service Partner Badges are reviewed periodically. Non-compliant partners at the time of the review will have one quarter to return to compliance.

### GitLab Professional Services Partner Requirements

![GitLab Professional Services Partner Badge](/images/channel-service-program/gitlab-professional-services-partner-badge.png)

<table>
   <tr>
      <td><h3>Program Entry Requirements</h3></td>
      <td>Each PSP must be:
      <ol>
   <li>Be a Open or Select GitLab Partner or GitLab Distribution Partner</li>


<li>Design, build and operate a professional service practice, and</li> 
       <li>Hire team members who have completed the competency requirements and/or sponsor the appropriate number of team members through completion of the competency requirements</li></ol>

   </td>

   </tr>
   
   <tr>
      <td><h3>Competency Requirements</h3>
      </td>
      <td>Each PSP must remain compliant with the either the Open, Select, or Distribution programs, and perpetually employ



      
at least three (3) <a href="https://about.gitlab.com/handbook/resellers/training/#gitlab-professional-services-engineer-pse">GitLab Certified Professional Service Engineers</a> 
      <ol>
         <li>Partner organizations who achieved their PSP prior to Nov. 4, 2021 are required to have three (3) <a href="https://about.gitlab.com/handbook/resellers/training/#gitlab-professional-services-engineer-pse">GitLab Certified Professional Service Engineers</a> on staff prior to November 1, 2022</li>
      </ol>
      </td>
  </tr>
  <tr>
     <td><h3>Service Offerings</h3>
     </td>
     <td>Any or all of the following services can be provided by an PSP: 
     <br>* Assessment
     <br>* Migration
     <br>* Integration
     <br>* Implementation
     <br>* Optimization
     <br>* Security/Compliance
     </td>
  </tr>
  <tr>
     <td><h3>Compliance Requirements</h3>
     </td>
     <td>Each PSP must:
     <br>* Hire and continually employ team members who achieve and maintain the competency requirements 
     <br>* Maintain positive Customer Success ratings measured against the following <a href="https://about.gitlab.com/handbook/customer-success/vision/#time-to-value-kpis">Time-to-Value KPIs</a> that GitLab uses for its metrics: Infrastructure Ready, First Value, & Outcome Achieved.
     </td>
     
  </tr>
</table>

### GitLab Training Service Partner Requirements 
Please reach out to [levelup@gitlab.com](levelup@gitlab.com)  to learn about the GilLab Training Services Partner program. 

<br>



## GitLab Channel Service Partner Program Benefits

There is significant business opportunity for partners to offer and deliver GitLab enabled services, and in order to ensure there is consistent customer experience throughout the market we offer GitLab Service Partner Badges.

Each Badge offers unique benefits that help partners better prepare themselves to deliver customer value through services directly to our joint customers. 

### Service Enablement Benefits

<table>
  <tr>
   <td colspan="2" >


   GitLab recognizes the important role our partners play in delivering value and services to our joint customers. To ensure partners have the latest sales and technical knowledge about our products, we offer many different training opportunities and technical resources. Partners can learn in a self-paced environment, we offer partners free access to the learning and exams for the required accreditations and certifications. 
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Training Benefits and Technical Resources
   </td>
  </tr>
  <tr>
   <td>Free Exams for required GitLab Certifications. 
   </td>
  </tr>
  <tr>
   <td >GitLab Badges: Recognizes customer success performance for your organization. 
   </td>
  </tr>
  <tr>
   <td>Certifications: Recognizes technical capabilities among your delivery teams.
   </td>
  </tr>
  <tr>
  <td rowspan="3" >Service and Technical Enablement Resources
   </td>
   <td>Service Packages: GitLab Services toolkits, sales and marketing IP
   </td>
  </tr>
  <tr>
   <td>Channel Solution Architect guidance
   </td>
  </tr>
  <tr>
  <td>Customer success adoption workshop kits </td>
  </tr>
</table>
  

### Marketing Benefits

<table>
  <tr>
   <td colspan="2" >


   GitLab Select partners have access to the proposal-based <a href="https://about.gitlab.com/handbook/resellers/#the-marketing-development-funds-mdf-program">GitLab Marketing Development Funds (MDF) Program</a>, which provides funding support for eligible marketing and partner enablement activities. Select partners who are also GitLab Service Partners will have the highest visibility in the market. 
   </td>
  </tr>
  <tr>
   <td >Services Related Marketing Benefits
   </td>
   <td>Partner Locator Badges:  find partners with capabilities to address and deliver specific outcomes.
   </td>
  </tr>
</table>
  

### Sales Acceleration

<table>
  <tr>
   <td colspan="2" >


   All authorized GitLab partners are eligible for co-selling with the GitLab Sales team. Service Partners are often prioritized in co-selling activities because those partners are able to deliver greater value for our joint customers. GitLab Service Partners can be brought in for services-only opportunities to help customers assess their DevOps infrastructure in advance of a potential GitLab purchase, or optimize their GitLab deployments.
   </td>
  </tr>
  <tr>
   <td>Sales Acceleration
   </td>
   <td>Opportunities for co-delivering services
   </td>
  </tr>
</table>



<br>





## Maintaining Service Partner Badges

At GitLab, collaboration and feedback is meant to nurture and mentor our service partners to grow their service capabilities and expertise. The program will utilize our internal [customer success metrics](https://about.gitlab.com/handbook/customer-success/vision/#measurement-and-kpis) to measure and understand the GitLab customer experience across the entire ecosystem and help further develop our cservice partners. In order to work together in this process, we will ask each service partner to provide evidence (as stated in the program descriptions above) to support the periodic  review  for your customer set one month before it is due. 

GitLab Channel Partner program will review the GitLab Service Partners’ customer success rating and practitioner certification status each year in the month the partner was originally granted certification. Partners will be notified with the outcome of the review and the resulting status of their badge. If the partner needs to make further investments to stay in good standing with regards to Service Partner badge, the email will indicate the existing status and state the expected status with a due date when compliance is required to renew your GitLab Service Partner Badge. Partners can work with their GitLab Channel Account Manager (CAM) to create a plan for obtaining and maintaining badges.
