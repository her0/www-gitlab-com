---
layout: handbook-page-toc
title: "IT Program Management Office"
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fas fa-bullseye" id="biz-tech-icons"></i> Our Mission

The IT Program Management Office (PMO) mission is to provide:
- A standard approach to project delivery across the IT department
- Full and accurate visibility of IT project status
- Effective prioritization of project management resources to support all IT initiatives

# <i class="fas fa-users" id="biz-tech-icons"></i> IT PMO Team

We are a global team of five, focused on building and managing world-class information technology systems and business processes. To learn more about our individual job functions, visit the [Program Management, Business Technology family page](https://handbook.gitlab.com/job-families/finance/program-management-bt/).

**Barbara Roncato - Manager, IT Program Management**  
GitLab handle: [@broncato](https://gitlab.com/broncato)  
Slack handle: `@barbara`   
Location and Timezone: Portugal, WEST/GMT+1  
LinkedIn Profile: [/roncatobarbara](https://www.linkedin.com/in/roncatobarbara/)

**Caroline Swanson - Senior IT Program Manager**  
GitLab handle: [@caroline.swanson](https://gitlab.com/caroline.swanson)  
Slack handle: `@Caroline Swanson`   
Location and Timezone: USA, MDT/MST  
LinkedIn Profile: [/caroline-smoke-swanson-pmp-a869b685](https://www.linkedin.com/in/caroline-smoke-swanson-pmp-a869b685/)

**Kayoko Cooper - Business Systems Analyst**  
GitLab handle: [@kayokocooper](https://gitlab.com/kayokocooper)  
Slack handle: `@Kayoko Cooper` <br>
Location and Timezone: USA, EDT/EST  
LinkedIn Profile: [/kayoko-cooper-a1063522](https://www.linkedin.com/in/kayoko-cooper-a1063522/)

**Kristina Koceban - Senior Business Systems Analyst**  
GitLab handle: [@kkoceban](https://gitlab.com/kkoceban)  
Slack handle: `@Kristina`   
Location and Timezone: Ireland, WEST/GMT+1  
LinkedIn Profile: [/kristinakoceban](https://www.linkedin.com/in/kristinakoceban/)

**Nico Sandoval - Business Systems Analyst**  
GitLab handle: [@nicosando](https://gitlab.com/nicosando)  
Slack handle: `@Nico Sandoval` <br>
Location and Timezone: USA, EDT/EST  
LinkedIn Profile: [/nicolas-sandoval-331900b3](https://www.linkedin.com/in/nicolas-sandoval-331900b3/)

# <i class="fas fa-tasks" id="biz-tech-icons"></i> What We Do

## Portfolio Planning & Prioritization

![it-pmo](/handbook/business-technology/pmo/it-pmo-planning.png)

The IT portfolio planning & prioritization process has 6 steps:

1. Portfolio of Projects
2. Prioritization
3. T-Shirt Sizing
4. Resources & Capacity Planning
5. Project Commits
6. Alignment & OKRs

### Portfolio of Projects

The inventory of all committed and planned projects in IT Operations and Enterprise Applications. The [IT - Consolidated Projects Roadmap](https://docs.google.com/spreadsheets/d/1ojnTFGB2iYJz9kNPYdoLc4PeyDqAN60gu9cQBrKpHJQ/edit?pli=1#gid=955721517) is available for internal team members to track IT planned initiatives. All projects on the roadmap have a Team & Corporate stack ranked to align with the overall [global optimization](https://about.gitlab.com/handbook/values/#global-optimization). 

### Prioritization

The [IT Prioritization tracker](https://docs.google.com/spreadsheets/d/1fnV3nWI0sNdbakHUys78d-lpbpKexFD_1kdO8JoVySY/edit#gid=157883866) is used to log new projects / initiatives with the IT team. The IT PMO team will review it as part of the Quarterly Portfolio Planning:

![it-pmo](/handbook/business-technology/pmo/quarterly-planning.png)

### T-Shirt Sizing

Consists in identifying the project's:
- Estimated Level Of Effort (LOE)
- Duration
- Roles & Responsibilities

### Resources & Capacity Planning

Achieved by:
- Identifying all resource on the team
- Estimating capacity like Keeping the lights on (KTLO), Admin, Management and Projects

### Project Commits

All projects added to the [IT - Consolidated Projects Roadmap](https://docs.google.com/spreadsheets/d/1ojnTFGB2iYJz9kNPYdoLc4PeyDqAN60gu9cQBrKpHJQ/edit?pli=1#gid=955721517) have:
- Resources assigned in order of priority
- An appropriated label:
   - **Committed**: Projects that are in-flight / being worked on
   - **Planned**: Projects that will be picked up once the resources from a committed project becomes available
   - **Backlog**: Projects that are in the pipeline but not yet ready to start

### Alignment & OKRs

All projects added to the [IT - Consolidated Projects Roadmap](https://docs.google.com/spreadsheets/d/1ojnTFGB2iYJz9kNPYdoLc4PeyDqAN60gu9cQBrKpHJQ/edit?pli=1#gid=955721517) are part of the Business Technology [OKRs](https://about.gitlab.com/company/okrs/) for: 
- Awareness
- Accountability
- Value Generation

## Release Management

The release management plan helps the IT team:
- Set an agreed upon standard 
- Operationalize 
- Keep consistency & predictability 
- Communicate changes regularly and transparently

### Release Calendar

![it-pmo](/handbook/business-technology/pmo/calendar.png)

# <i class="fas fa-bullhorn" id="biz-tech-icons"></i> How We Work

## System Development Lifecycle (SDLC)

The IT team follows the Software Development Lifecycle to provide a framework of activities performed at each stage of our projects.

![it-pmo](/handbook/business-technology/pmo/sdlc.png)

### Intake

The intake process starts with the completion of a business requirements document. It is the bridge between business stakeholders defining what is to be worked on and the development group that will build it.

### Define

During Define, the project team articulates the business problem and definition of done. This phase covers uses cases, technology impacted and requirements. The ultimate goal of Define is not to figure out everything but to document the process and what is impacted. Finally, before beginning the design, we request approval sign-off from key stakeholders to ensure business requirements are accurate and all-inclusive.

### Design

This phase creates the roadmap of how things will work in the future. The project team work with the business to design a future state process and refine the requirements to build it. This phase should iron out all the details and questions from above. 

### Build

In the Build phase, the project team collaborates with other stakeholders to develop solutions that meet the business requirements but also that are scalable and aligned with best-practices.

### Test

During the test phase, the project team collaborates with other stakeholders to develop test scripts and facilitates the user acceptance testing (UAT) process.

### Deploy

The project team owns developing a deployment plan and collaborating with other implementation teams to ensure completeness. This involves launch plans, user enablement, and go-live communications.

### Hypercare

Hypercare is the period that immediately follows the deployment / launch of a new process in Production where an elevated level of support is available to ensure the seamless adoption of a new system.

### Maintenance

The maintenance phase occurs once the new process is fully operational to ensure it continues to function as it was designed to, and repairs or upgrades are performed as needed.

## Governance and Program Management Methodology

Check the [Governance and IT Program Management](/handbook/business-technology/pmo/pmo-governance/) page to understand the methodology that the IT PMO team follows for all GitLab IT programs in order to:
- ensure strong results and execution in an efficient way
- proper scoping consistent with our value of iteration
- correct stakeholders consistent with our value of collaboration and proper documentation for compliance consistent with our transparency value

## Weekly Roll Up Reporting

The IT Integrations team developed the [Rolly Bot](/handbook/business-technology/how-we-work/rolly/) to automate the creation and dissemination of weekly status updates. By using their tool, the IT PMO Team is able to provide regular updates about on going projects. One roll up is generated every Monday.

# <i class="fas fa-file-alt" id="biz-tech-icons"></i> Meetings

## IT Governance

### IT Leadership Meeting
- **Purpose**: Every week the IT Leadership team has a dedicated time to sync up and discuss current activities (urgent and important) that impacts the whole team.
   - **Frequency**: Weekly
      - The time of this meeting may vary to accommodate the different timezones of the IT / Business Technology team members.
   - **Participants**: VP, IT (host), IT PMO, Enterprise Applications Leadership, IT Operations Leadership.

### IT Management Team Meetings
- **Purpose**: Every week each IT functional team has a dedicated time to sync up and discuss current activities (urgent and important) that impacts the whole team.
   - **Frequency**: Weekly
      - The time of this meeting may vary to accommodate the different timezones of the IT / Business Technology team members.
   - **Participants**: IT Functional Teams.

### OKR Review
- **Purpose**: Every month the IT Leadership team has a dedicated time to sync up and discuss current activities (urgent and important) that impacts the whole team.
   - **Frequency**: Monthly
   - **Participants**: VP, IT (host), IT PMO, Enterprise Applications Leadership, IT Operations Leadership.

### IT Key Review
- **Purpose**: Key review meetings allow a functional group to stay updated on and discuss essential success measurements, such as: OKRs, KPIs, how the team is trending toward achieving goals, blocked tasks, new assignments, workstream changes, etc.
   - **Frequency**: Quarterly
   - **Participants**: VP, IT (host), all GitLab. 

### IT All Hands
- **Purpose**: Share company updates, stay connected, and receive feedback.
   - **Frequency**: Quarterly
   - **Participants**: VP, IT (host), all IT cross functional teams.

## Stakeholder Management

### Project Status Meetings
- **Purpose**: During this meeting the IT PMO team uses [Rolly](https://gitlab.com/gitlab-com/business-technology/business-technology-ops/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=EntApps-weekly-rollup) to cover project status, what is in progress and what is blocked. 
   - **Frequency**: Monthly
   - **Participants**: IT PMO (host), Business DRI(s), Project Subject Matter Experts (SMEs)

### Portfolio Business Meeting
- **Purpose**: Provide project status, path to escalation, nurture relationships with business. During this meeting IT PMO team uses [Rolly](https://gitlab.com/gitlab-com/business-technology/business-technology-ops/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=EntApps-weekly-rollup) to cover project status, what is in progress and what is blocked. 
   - **Frequency**: Monthly
   - **Participants**: IT PMO (host), Business DRI(s).

### Portfolio Roadmap & Health Meeting
- **Purpose**: Discuss roadmap & timelines, shifts in priorities, alignment to goals. This is a strategic meeting to ensure that teams are aligned with their key business partners and have clarity on our shared roadmap.
   - **Frequency**: Quarterly
   - **Participants**: IT PMO (host), Business DRI(s), Business & IT Leadership.

### IT SteerCo 
- **Purpose**: Provide advice, ensure delivery of IT project outputs and the achievement of IT project outcomes.
   - **Frequency**: Quarterly
   - **Participants**: VP, IT (host), Business & IT Leadership.

### Budget Planning
- **Purpose**: Discuss the company's budget and make adjustments as needed.
   - **Frequency**: Annual
   - **Participants**: Business & IT Leadership.

## No-Meetings Fridays

While we can't promise we'll never have meetings on Fridays, the IT PMO team has adopted [No Meetings Fridays](/handbook/communication/).

## Meetings Protocol

The team follows [GitLab meeting practices and standards](https://about.gitlab.com/company/culture/all-remote/meetings/).

- Every meeting has an agenda.
- Meeting topics are listed in priority order.
- Notes are added to the agenda inline with topics and questions.
- All agendas are stored in the PMO Team Meetings [shared drive](https://drive.google.com/drive/folders/1nKgH2Q9PztXE388dbhUHxSh1Vy2V_vLz).

# <i class="fas fa-headset" id="biz-tech-icons"></i> How To Connect With Us

### IT PMO Slack Channels

- [#business-technology](https://gitlab.slack.com/archives/C01BLS12V37) is the primary channel for all business technology related conversations. This is where GitLab team members can link to their issues, ask for help, direction, and get general feedback from members of the Business Technology team.

### GitLab Groups and Projects

The IT PMO team primarily uses the below groups and projects on GitLab:

- [Program Management Office](https://gitlab.com/gitlab-com/business-technology/program-management-office) group: main group for the IT PMO team.



{::options parse_block_html="false" /}


